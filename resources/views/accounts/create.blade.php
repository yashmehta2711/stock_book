@extends('layouts.admin-panel.app')

@section('content')
    <div class="card">
        <div class="card-header"><h2>Add New Account</h2></div>
        <div class="card-body">
            <form action="{{ route('accounts.store') }}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="pan_number">Pan Number</label>
                    <input type="text"
                            class="form-control @error('pan_number') is-invalid @enderror"
                            id="pan_number"
                            value="{{ old('pan_number') }}"
                            placeholder="Enter Pan Number"
                            name="pan_number">
                        @error('pan_number')
                            <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                        @enderror
                </div>

                <div class="form-group">
                    <label for="aadhar_number">Aadhar Number</label>
                    <input type="text"
                            class="form-control @error('aadhar_number') is-invalid @enderror"
                            id="aadhar_number"
                            value="{{ old('aadhar_number') }}"
                            placeholder="Enter Aadhar Number"
                            name="aadhar_number">
                        @error('aadhar_number')
                            <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                        @enderror
                </div>

                <div class="form-group">
                    <label for="bank_name">Bank Name</label>
                    <input type="text"
                            class="form-control @error('bank_name') is-invalid @enderror"
                            id="bank_name"
                            value="{{ old('bank_name') }}"
                            placeholder="Enter Bank Name"
                            name="bank_name">
                        @error('bank_name')
                            <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                        @enderror
                </div>

                <div class="form-group">
                    <label for="bank_account_number">Bank Account Number</label>
                    <input type="text"
                            class="form-control @error('bank_account_number') is-invalid @enderror"
                            id="bank_account_number"
                            value="{{ old('bank_account_number') }}"
                            placeholder="Enter Bank Account Number"
                            name="bank_account_number">
                        @error('bank_account_number')
                            <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                        @enderror
                </div>
                <button type="submit" class="btn btn-outline-success">Submit</button>
            </form>
        </div>
    </div>
@endsection
