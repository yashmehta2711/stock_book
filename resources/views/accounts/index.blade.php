@extends('layouts.admin-panel.app')

@section('content')
    <div class="d-flex justify-content-end mb-3">
        <a href="{{ route('accounts.create') }}" class="btn btn-outline-primary">Add Account</a>
    </div>
    <div class="card">
        <div class="card-header"><h2>Accounts</h2></div>
        <div class="card-body">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Pan Number</th>
                        <th scope="col">Aadhar Number</th>
                        <th scope="col">Bank Name</th>
                        <th scope="col">Bank Account Number</th>
                        <th scope="col">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($accounts as $account)
                        <tr>
                            <td>{{ $account->pan_number }}</td>
                            <td>{{ $account->aadhar_number }}</td>
                            <td>{{ $account->bank_name }}</td>
                            <td>{{ $account->bank_account_number }}</td>
                            <td>
                                <a href="{{ route('accounts.edit', $account->id) }}" class="btn btn-sm btn-primary">Edit</a>
                                <button type="button" class="btn btn-sm btn-danger" onclick="displayModal({{ $account->id }})" data-toggle="modal" data-target="#deleteModal">
                                    Delete
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <!-- DELETE MODAL -->
    <div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteModalLabel">Modal Title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" method="POST" id="deleteAccountForm">
                    @csrf
                    @method('DELETE')
                    <div class="modal-body">
                        Are you sure, you want to delete this account?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-outline-danger">Delete Account</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="mt-5">
        {{-- {{ $accounts->links('vendor.pagination.bootstrap-4') }} --}}
    </div>
@endsection

@section('page-level-scripts')
    <script>
        function displayModal(accountId){
            var url = "/accounts/" + accountId;
            $("#deleteAccountForm").attr('action', url);
        }
    </script>
@endsection
