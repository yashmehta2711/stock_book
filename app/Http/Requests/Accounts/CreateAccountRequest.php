<?php

namespace App\Http\Requests\Accounts;

use Illuminate\Foundation\Http\FormRequest;

class CreateAccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pan_number'=>'required|unique:accounts',
            'aadhar_number'=>'required|unique:accounts|max:255',
            'bank_name'=>'required|min:3',
            'bank_account_number'=>'required|min:8|unique:accounts',
        ];
    }
}
