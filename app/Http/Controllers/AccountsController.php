<?php

namespace App\Http\Controllers;

use App\Http\Requests\Accounts\CreateAccountRequest;
use App\Http\Requests\Accounts\UpdateAccountRequest;
use App\Models\Account;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AccountsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $accounts = Account::paginate(10);
        return view('accounts.index', compact('accounts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('accounts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateAccountRequest $request)
    {
        Account::create([
            'user_id' => Auth::user()->id,
            'pan_number' => $request->pan_number,
            'aadhar_number' => $request->aadhar_number,
            'bank_name' => $request->bank_name,
            'bank_account_number' => $request->bank_account_number,
        ]);
        session()->flash('success', 'Account Created Successfully!');
        return redirect(route('accounts.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Account $account)
    {
        return view('accounts.edit', compact(['account']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAccountRequest $request, Account $account)
    {
        $account->pan_number = $request->pan_number;
        $account->aadhar_number = $request->aadhar_number;
        $account->bank_name = $request->bank_name;
        $account->bank_account_number = $request->bank_account_number;

        $account->save();

        session()->flash('success','Account updated successfully!');
        return redirect(route('accounts.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Account $account)
    {
        $account->delete();
        session()->flash('success', 'Account Deleted Successfully!');
        return redirect(route('accounts.index'));
    }
}
